from functools import wraps
import json
from datetime import datetime
from flask import Flask, jsonify, request, Response
app = Flask(__name__)
app.debug = True


'''
  Authorization part
'''

users =[]
users.append({"admin":"123"})
current_user = {}


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    current_user = {username: password} in users
    return current_user


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


'''
 Data models
'''


class Comment:
    last_id = 0

    def __init__(self, creator, text):
        # self.board_id = Comment.last_id
        Comment.last_id += 1
        self.creator = creator
        self.text = text
        self.creation_date = datetime.now()

class Like:
    def __init__(self, creator):
        self.creator = creator
        self.creation_date = datetime.now()


class Board:
    last_id = 0

    def __init__(self, creator, title):
        self.board_id = Board.last_id
        Board.last_id += 1
        self.creator = creator
        self.title = title
        self.creation_date = datetime.now()
        self.comments = []
        self.likes = []

    def add_comment(self, creator, text):
        users_comments = [comment for comment in self.comments if comment.creator == creator]
        if len(users_comments) >= 5:
            if (datetime.now() - users_comments[0].creation_date).seconds < 3600:
                return "Comments limitation"
            else:
                self.comments.remove(self.comments[0])
                return self.comments[len(self.comments) - 1]

        self.comments.append(Comment(creator, text))
        return self.comments[len(self.comments) - 1]

    def add_like(self, creator):
        users_likes = [like for like in self.likes if like.creator == creator]
        if len(users_likes) >= 5:
            if (datetime.now() - users_likes[0].creation_date).seconds < 3600:
                return "Likes limitation"
            else:
                self.likes.remove(self.likes[0])
                return self.likes[len(self.likes) - 1]

        self.likes.append(Like(creator))
        return self.likes[len(self.likes) - 1]


boards = [
    Board('Margo', 'New board'),
    Board('Anonnymus', 'New board')
]


def convert(obj):
    if isinstance(obj, datetime):
        return str(obj)
    return obj.__dict__


@app.route("/api/boards/signup/", methods=['POST'])
def view_board_signup():
    content = request.get_json()
    if len(content["username"]) >20 or len(content["password"]) > 20 or len(content["confirm_password"])>20:
        return "Invalid input"
    if not content["password"] == content["confirm_password"]:
        return "Passwords doesn`t match"
    new_user = {content["username"]: content["password"]}
    users.append(new_user)
    return str(new_user)


# get board list
@app.route("/api/boards/", methods=['GET'])
@requires_auth
def view_boards():
    return jsonify({'boards': [json.dumps(board.__dict__) for board in boards]})


@app.route("/api/boards/<int:board_id>", methods=['GET'])
@requires_auth
def view_board_get(board_id):
    return jsonify(json.dumps(([board
                             for board in boards if board.board_id == board_id][0].__dict__), default=convert
                              ))


@app.route("/api/boards/new/", methods=['POST'])
@requires_auth
def view_board_add():
    content = request.get_json()
    if len(content["creator"]) >20 or content["title"] > 50:
        return "Invalid input"
    new_board = Board(content["creator"],content["title"])
    boards.append(new_board)
    return str(new_board.board_id)


@app.route("/api/boards/<int:board_id>/add_comment/", methods=['POST'])
@requires_auth
def view_comment_add(board_id):
    board = [board for board in boards if board.board_id == board_id][0]
    content = request.get_json()
    if len(content["creator"]) > 20 or len(content["comment"]) > 255:
        return "Invalid input"

    return str(board.add_comment(content["creator"], content["comment"]))


@app.route("/api/boards/<int:board_id>/add_like/", methods=['POST'])
@requires_auth
def view_like_add(board_id):
    board = [board for board in boards if board.board_id == board_id][0]
    content = request.get_json()
    return str(board.add_like(content["creator"]))


@app.route("/")
@requires_auth
def hello():
    return "This is Board."